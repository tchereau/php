<?php
require("fonction.php");

$clefs=array('id', 'nom', 'prenom');
if (exist('id') || exist('nom') || exist('prenom')) {
    //vérification de sécu
    if (verifNumericEntier($_GET['id'])) {
        $id=$_GET['id'];
    }
    if (verifAlpha($_POST['nom']) && verifAlpha($_POST['prenom'])) {
        $nom=$_POST['nom'];
        $prenom=$_POST['prenom'];
    }
    if(isset($nom)){
        $users=seeUser('', $nom);
    }
}
else {
    $users=seeAllUser();
}
//else {
//    $users=seeAllUser();
//}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP BDD</title>
</head>
<body>
    <h1>tableau des utilisateurs</h1>
    <table>
        <thead>
            <tr>
                <th>id</th>
                <th>nom</th>
                <th>prénom</th>
                <th>numéro</th>
                <th>rue</th>
                <th>code postal</th>
                <th>ville</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody> 
            <?php

            foreach($users as $user):
            ?>

            <tr>
            <td><?= $user['id'] ?></td>
            <td><?= $user['nom'] ?></td>
            <td><?= $user['prenom'] ?></td>
            <td><?= $user['nom_rue'] ?></td>
            <td><?= $user['num_rue'] ?></td>
            <td><?= $user['cp'] ?></td>
            <td><?= $user['ville'] ?></td>
            <td><?= $user['email'] ?></td>
            <td><a href="update.php?id=<?= $user['id'] ?>">Modifier</a></td>
            <td><a href="delete.php?id=<?= $user['id'] ?>">Supprimer</a></td>

            </tr>
            <?php
                endforeach;
            ?>
        </tbody>
        
    </table>
    <a href="ajout.php">ajouter un utilisateur</a>
</body>
</html>