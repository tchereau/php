<?php
require("fonction.php");

if (exist('id') && verifNumericEntier($_GET['id'])) {
    $id=$_GET['id'];
    deleteUser($id);
    echo "utilisateur supprimé !";
}
if (exist($_POST['id']) && verifNumericEntier($_POST['id'])){
    $id=$_POST['id'];
    deleteUser($id);
    echo "utilisateur supprimé !";
}
$users=seeAllUser();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>SUPPRIMER UN UTILISATEUR</h1>
<table>
        <thead>
            <tr>
                <th>id</th>
                <th>nom</th>
                <th>prénom</th>
                <th>numéro</th>
                <th>rue</th>
                <th>code postal</th>
                <th>ville</th>
                <th>Email</th>
                <th>test</th>
            </tr>
        </thead>
        <tbody> 
            <?php

            foreach($users as $user):
            ?>
            <tr>
            <td><?= $user['id'] ?></td>
            <td><?= $user['nom'] ?></td>
            <td><?= $user['prenom'] ?></td>
            <td><?= $user['nom_rue'] ?></td>
            <td><?= $user['num_rue'] ?></td>
            <td><?= $user['cp'] ?></td>
            <td><?= $user['ville'] ?></td>
            <td><?= $user['email'] ?></td>
            <td><?php echo '<form action="" method="post"><button name="id" value="'.$user['id'].'"type="submit">Ne PAS supprimer</button></form>' ?></td>
            </tr>
            <?php
                endforeach;
            ?>
        </tbody>
        
    </table>




    <!--form action="" method="post">
    <label for="id">nom</label>
    <input type="text" name="id">
    <button value="" type="submit">Ne PAS supprimer</button>
    </form>
    <a href=".">Pas de Retour</a-->
</body>
</html>