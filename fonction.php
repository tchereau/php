 <?php
require_once('credential.php');
function dbConnect(){
    try{
        $sql= 'mysql:host=localhost;port=3307;dbname=php_bdd;charset=utf8';
        $pdo = new PDO($sql, $GLOBALS['username'], $GLOBALS['password']);
        $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }
    //vérification de la connexion
    catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    } 
}

function adduser($nom, $prenom, $nomRue, $numRue = 0, $cp, $ville,$email){
    $pdo = dbConnect();
    $adduser = $pdo->prepare("INSERT INTO utilisateur VALUES(0,:nom,:prenom,:nomRue,:numRue,:cp,:ville,:email)");
    $adduser -> bindParam(':nom', $nom);
    $adduser -> bindParam(':prenom', $prenom);
    $adduser -> bindParam(':nomRue', $nomRue);
    $adduser -> bindParam(':numRue', $numRue);
    $adduser -> bindParam(':cp', $cp);
    $adduser -> bindParam(':ville', $ville);
    $adduser -> bindParam(':email', $email);
    
    $adduser->execute();
}

function seeAllUser(){
    $pdo = dbConnect();
    $seeAllUser = $pdo->prepare("SELECT id, nom, prenom, nom_rue, num_rue, cp, ville, email FROM utilisateur ORDER BY id");
    
    $seeAllUser->execute();
    $result = $seeAllUser->fetchAll();
    return $result;
}

function seeUser($id='', $nom='', $prenom='', $nomRue='', $numRue = 0, $cp=0, $ville='',$email=''){
    $pdo = dbConnect();
    $sqlCommande="SELECT id, nom, prenom, nom_rue, num_rue, cp, ville, email FROM utilisateur where 1=1 ";

    if ($id != ''){
        $sqlCommande.="AND id LIKE :id ";
    }elseif ($nom!= ''){
        $sqlCommande.="AND nom LIKE :nom ";
    }elseif ($prenom!= ''){
        $sqlCommande.="AND prenom LIKE :prenom ";
    }elseif ($nomRue!= ''){
        $sqlCommande.="AND nomRue LIKE :nomRue ";
    }elseif ($numRue!= ''){
        $sqlCommande.="AND numRue=:numRue ";
    }elseif ($cp!= ''){
        $sqlCommande.="AND cp=:cp ";
    }elseif ($ville!= ''){
        $sqlCommande.="AND ville LIKE :ville ";
    }elseif ($email!= ''){
        $sqlCommande.="AND email LIKE :email ";
    }
    $seeuser = $pdo->prepare($sqlCommande);

    if ($id != ''){
        $seeuser -> bindParam(':id', $id);
    }elseif ($nom!= ''){
        $seeuser -> bindParam(':nom', $nom);
    }elseif ($prenom!= ''){
        $seeuser -> bindParam(':prenom', $prenom);
    }elseif ($nomRue!= ''){
        $seeuser -> bindParam(':nomRue', $nomRue);
    }elseif ($numRue!= ''){
        $seeuser -> bindParam(':numRue', $numRue);
    }elseif ($cp!= ''){
        $seeuser -> bindParam(':cp', $cp);
    }elseif ($ville!= ''){
        $seeuser -> bindParam(':email', $email);
    }elseif ($email!= ''){
        $seeuser -> bindParam(':email', $email);
    }

    $seeuser->execute(/*array(':nom' => $nom . '%')*/);
    $result= $seeuser->fetchAll();
    return $result;
}


function exist($clef){
    if(!isset($_POST[$clef]) && $_POST[$clef] == ''){
        if(!isset($_GET[$clef]) && $_GET[$clef] == ''){
            return false;
        }
    }
    return true;
}

function existArray($clefs){
    foreach($clefs as $clef){
        if(!isset($_POST[$clef]) && $_POST[$clef] == ''){
            if(!isset($_GET[$clef]) && $_GET[$clef] == ''){
                return false;
            }
        }
    }
    return true;
}
function addData($base, $table){
    $pdo = dbConnect();
    $addData = $pdo->prepare("INSERT INTO $base VALUES ('".implode("' , '", array_values($table))."')");
    $addData->execute();
}
function seeid(){
    $pdo = dbConnect();
    $seeid = $pdo->prepare("SELECT id, nomUtilisateur, mdp FROM identifiant");
    $seeid->execute();
    $result = $seeid->fetchAll();
    return $result;
}
function deleteUser($iduser){
    $pdo = dbConnect();
    $del= $pdo->prepare("DELETE FROM utilisateur WHERE id=:iduser");
    $del -> bindParam(':iduser', $iduser);
    $del->execute();
}
function connexion($username){
    $pdo = dbConnect();
    $seeid = $pdo->prepare("SELECT nomUtilisateur, mdp FROM identifiant WHERE nomUtilisateur LIKE :username");
    $seeid -> bindParam(':username', $username);
    $seeid->execute();
    $result= $seeid->fetchAll();
    return $result;
}

function updateUserName($up, $id, $nom='',$prenom='')
{
    if(isset($up) && $up==false){
        $pdo = dbConnect();
        $user = $pdo->prepare("SELECT id, nom, prenom FROM utilisateur WHERE id=:id");
        var_dump($id); //fonctionne
        $user -> bindParam(':id', $id);
        $user->execute();
        $result = $user->fetchAll();
        return $result;
    }elseif((isset($up) && $up==true && $nom!='' && $prenom!='')){
        $pdo = dbConnect();
        $updateNomPrenom = $pdo->prepare("UPDATE utilisateur SET nom=:nom, prenom=:prenom WHERE id=:id");
        $updateNomPrenom -> bindParam(':id', $id);
        $updateNomPrenom -> bindParam(':nom', $nom);
        $updateNomPrenom -> bindParam(':prenom', $prenom);
        $updateNomPrenom->execute();
        $updateNomPrenom->fetch();
        return $updateNomPrenom;
    }
}

function verifAlpha($alpha){
    if (ctype_alpha($alpha)){
        return true;
    }else
    {
        return false;
    }
}
function verifNumericEntier($numeric){
    if (ctype_digit($numeric)){
        return true;
    }else
    {
        return false;
    }
}